const elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */




//Change laravel elixir public path
//elixir.config.assetsPath = "assets";
elixir.config.publicPath = "assets/dashboard";

elixir(mix => {
mix.copy('node_modules/angular-confirm1/css/angular-confirm.css', 'resources/assets/sass/import/_angular-confirm.scss');
mix.copy('node_modules/angular-loading-bar/build/loading-bar.css', 'resources/assets/sass/import/_loading-bar.scss');
mix.copy('node_modules/lightbox2/dist/css/lightbox.css', 'resources/assets/sass/import/_lightbox.scss');
mix.copy('node_modules/lightbox2/dist/images/**.*', 'assets/dashboard/images');

mix.sass('app.scss');
mix.copy('resources/assets/vendor/bootstrap/fonts', 'assets/dashboard/fonts');
mix.copy('resources/assets/vendor/font-awesome/fonts', 'assets/dashboard/fonts');
mix.styles([
    'resources/assets/vendor/bootstrap/css/bootstrap.css',
    'resources/assets/vendor/animate/animate.css',
    'resources/assets/vendor/font-awesome/css/font-awesome.css',
], 'assets/dashboard/css/vendor.css', './');
mix.scripts([
    'resources/assets/vendor/jquery/jquery-3.1.1.min.js',
    'resources/assets/vendor/bootstrap/js/bootstrap.js',
    'resources/assets/vendor/metisMenu/jquery.metisMenu.js',
    'resources/assets/vendor/slimscroll/jquery.slimscroll.min.js',
    //'resources/assets/vendor/pace/pace.min.js',
    'resources/assets/js/app.js',
    'resources/assets/js/jasny/jasny-bootstrap.min.js',
    'node_modules/lightbox2/dist/js/lightbox.js',

    'node_modules/angular/angular.js',
    'resources/assets/js/angular/*.js',
    'node_modules/angular-route/angular-route.js',
    'node_modules/angular-resource/angular-resource.js',
    'node_modules/angular-animate/angular-animate.js',
    'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
    'node_modules/ng-file-upload/dist/ng-file-upload.js',
    'node_modules/ng-file-upload/dist/ng-file-upload-shim.js',
    'node_modules/angular-loading-bar/build/loading-bar.js',
    'node_modules/angular-confirm1/js/angular-confirm.js',


    'node_modules/angular-datatables/dist/angular-datatables.js',
    'node_modules/angular-datatables/dist/plugins/buttons/angular-datatables.buttons.js',
    'node_modules/angular-datatables/dist/plugins/colreorder/angular-datatables.colreorder.js',
    'node_modules/angular-datatables/dist/plugins/fixedheader/angular-datatables.fixedheader.js',
    'node_modules/angular-datatables/dist/plugins/fixedheader/angular-datatables.fixedheader.js',

    'resources/assets/js/dataTables/datatables.min.js',
    'resources/assets/js/angular/services/*.js',
    'resources/assets/js/angular/controller/*.js'


], 'assets/dashboard/js/app.js', './');

});
