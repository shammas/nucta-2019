<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="index, follow"/>
    <meta property="og:title" content="NUCTA is an inception for architectural and interior design solutions" />
    <meta property="og:site_name" content="NUCTA Architectural and Interior design solutions" />
    <meta property="og:url" content="http://nuctaweb.com/" />
    <meta property="og:description" content="NUCTA is an inception for architectural and interior design solutions started in 2016, by a team of three interior designers zealous about architecture, striving to fulfill your demands and dreams. It also consists of a young team of managers, supervisors and visualizers. The firm also handles furniture design and landscaping works." />
    <meta property="og:type" content="website" />
    <title>Nucta | architecture + interiors</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="NUCTA is an inception for architectural and interior design solutions started in 2016, by a team of three interior designers zealous about architecture, striving to fulfill your demands and dreams. It also consists of a young team of managers, supervisors and visualizers. The firm also handles furniture design and landscaping works.">
    <meta name="keywords" content="Nucta wandoor, Nucta Calicut, Nucta Qatar, Builders in Wandoor, Builders in Calicut, Builders in Qatar, Builders in Malappuram, Builders in Kearala, ready to occupy villas in Wandoor, ready to occupy villas in Calicut, ready to occupy villas in Qatar, ready to occupy villas in Kearala, budget interiors in Wandoor, budget interiors in Calicut, budget interiors in Qatar, budget interiors in Kearala" />
    <link rel="shortcut icon" href="assets/images/favicon.jpg">
    <!--=============== css  ===============--> 
    <link type="text/css" rel="stylesheet" href="assets/css/reset.css">
    <link type="text/css" rel="stylesheet" href="assets/css/plugins.css">
    <link type="text/css" rel="stylesheet" href="assets/css/style.css">
    <link type="text/css" rel="stylesheet" href="assets/css/yourstyle.css">
</head>
<body>
    <div class="loader">
        <div class="tm-loader">
            <div id="circle"></div>
        </div>
    </div>
    <div id="main">
        <header>
            <div class="nav-button">
                <span  class="nos"></span>
                <span class="ncs"></span>
                <span class="nbs"></span>
            </div>
            <div class="logo-holder">
                <a href="/" class="ajax"><img src="assets/images/logo.png" alt=""></a>
            </div>
            <div class="header-title">
                <h2><a class="ajax" href="#"></a></h2>
            </div>			
        </header>
        <div id="wrapper">
            <div class="content-holder elem scale-bg2 transition3">
                <div class="dynamic-title">About us</div>
                <div class="nav-overlay"></div>
                <div class="nav-inner isDown">
                    <nav>
                        <ul>
                            <li><a href="/" class="ajax"> Home </a></li>
                            <li><a href="about" class="ajax active"> About Us </a></li>
                            <li class="subnav">
                                <a href="portfolio" class="ajax"> Projects </a>
                                <ul>
                                    <?php
                                    if (isset($countries) and $countries != false) {
                                        foreach ($countries as $country) {
                                            ?>
                                            <li><a href="<?= base_url('portfolio-cat/' . $country->id . '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', character_limiter($country->country, 50,'')), '-');?>" class="ajax"> Projects in <?= $country->country;?> </a></li>
                                        <?php
                                        }
                                    }
                                    ?>
                                </ul>		
                            </li>
                            <li><a href="enquire" class="ajax"> Enquire Now </a></li>
                            <li><a href="contact" class="ajax"> Contact Us </a></li>
                        </ul>
                    </nav>
                </div>
                <!--=============== Content ===============-->
                <div class="content">
                    <section>
                        <div class="container-fluid">
                            <div class="services-holder">
                                <div class="services-item">
                                    <div class="serv-img lft-img">
                                        <div class="bg" style="background-image:url(assets/images/about.jpg)"></div>
                                    </div>
                                    <div class="services-box-info rft-info">
                                        <h4>NUCTA Architectural & Interior design solutions</h4>
                                        <p>
                                            Architecture to us is like peering through a kaleidoscope: there’s always something new, something better, something exciting and most importantly never the same.    To us the process of design, and not the final product, is of utmost importance as our visions and dreams shape and reshape themselves throughout the operation, giving birth to a design which is ethereal, original and a paragon in itself.
                                        </p>
                                        <p>
                                            We believe in a language which, at the same time, is dynamic and static, calm and turbulent, technical and artistic, contemporary and yet drawing its inspiration from the principles of yesteryears. We believe in designing spaces which amalgamates movement and stillness and an architecture which proclaims a motionless dance.
                                        </p>
                                    </div>
                                </div>
                                <div class="services-item">
                                    <div class="serv-img rft-img">
                                        <div class="bg" style="background-image:url(assets/images/about.png);opacity: .3"></div>
                                    </div>
                                    <div class="services-box-info lft-info">
                                        <!-- <h4>We deduce that the work of an architect can never end...</h4> -->
                                        <p>
                                            We deduce that the work of an architect can never end and the architecture can never be a finished product, as the spaces rework, redesign and re-evolve themselves with the people living in it.We practice this art of designing spaces with a passion in our hearts and a persistent mind to give you an ideal and meaningful domain.
                                        </p>
                                        <p>
                                            NUCTA is an inception for architectural and interior design solutions started in 2016, by a team of three interior designers zealous about architecture, striving to fulfill your demands and dreams. It also consists of a young team of managers, supervisors and visualizers. The firm also handles furniture design and landscaping works.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="container">
                            <h2 class="title-head">Our Team</h2>
                            <!-- <p class="sub-title">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do<br/> eiusmod
                                tempor incididunt ut
                            </p> -->
                            <div class="team-cover">
                                <?php
                                if (isset($teams) and $teams != false) {
                                    foreach ($teams as $team) {
                                        if ($team->file_name != '') {
                                            ?>
                                            <div class="single-team">
                                                <img src="<?= $team->url ?>" class="team-image">

                                                <div class="team-overlay">
                                                    <div class="team-name"><?= $team->name; ?></div>
                                                    <div class="team-desi"><?= $team->designation; ?></div>
                                                </div>
                                            </div>
                                        <?php
                                        }else{ ?>
                                            <div class="single-team">
                                                <img src="assets/images/noImage.jpg" class="team-image">
                                                <div class="team-overlay">
                                                    <div class="team-name"><?= $team->name;?></div>
                                                    <div class="team-desi"><?= $team->designation;?></div>
                                                </div>
                                            </div>
                                        <?php }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <footer>
                <div class="policy-box">
                    <span>&#169; NUCTA 2019  /  Designed by <a href="http://cloudbery.com/" target="_blank"><img src="assets/images/cloudbery.png" alt="Cloudbery Solutions"></a></span>
                </div>
                <div class="footer-social">
                    <ul>
                        <li><a href="https://www.facebook.com/Nuctaac/" target="_blank" ><i class="fa fa-facebook"></i><span>facebook</span></a></li>
                        <li><a href="https://www.instagram.com/nucta_architecture/" target="_blank" ><i class="fa fa-instagram"></i><span>instagram</span></a></li>
                        <!-- <li><a href="#" target="_blank"><i class="fa fa-twitter"></i><span>twitter</span></a></li>
                        <li><a href="#" target="_blank" ><i class="fa fa-pinterest"></i><span>pinterest</span></a></li>
                        <li><a href="#" target="_blank" ><i class="fa fa-tumblr"></i><span>tumblr</span></a></li> -->
                    </ul>
                </div>
            </footer>
        </div>
    </div>
    <!-- Main end -->
    <!--=============== google map ===============-->
    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY" type="text/javascript"></script>
    <!--=============== scripts  ===============-->
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins.js"></script>
    <script type="text/javascript" src="assets/js/core.js"></script>
    <script type="text/javascript" src="assets/js/scripts.js"></script>
</body>
</html>