<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="index, follow"/>
    <meta property="og:title" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="http://nuctaweb.com/" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <title></title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="">
    <meta name="keywords" content="" />
    <link rel="shortcut icon" href="assets/images/favicon.jpg">
    <!--=============== css  ===============--> 
    <link type="text/css" rel="stylesheet" href="assets/css/reset.css">
    <link type="text/css" rel="stylesheet" href="assets/css/plugins.css">
    <link type="text/css" rel="stylesheet" href="assets/css/style.css">
    <link type="text/css" rel="stylesheet" href="assets/css/yourstyle.css">
</head>
<body>
    <div class="loader">
        <div class="tm-loader">
            <div id="circle"></div>
        </div>
    </div>
    <div id="main">
        <header>
            <div class="nav-button">
                <span  class="nos"></span>
                <span class="ncs"></span>
                <span class="nbs"></span>
            </div>
            <div class="logo-holder">
                <a href="/" class="ajax"><img src="assets/images/logo.png" alt=""></a>
            </div>
            <div class="header-title">
                <h2><a class="ajax" href="#"></a></h2>
            </div>     
        </header>  
        <div id="wrapper">
            <div class="content-holder elem scale-bg2">
                <div class="dynamic-title">Home</div>
                <div class="nav-overlay"></div>
                <div class="nav-inner isDown">
                    <nav>
                        <ul>
                            <li><a href="/" class="ajax active"> Home </a></li>
                            <li><a href="about" class="ajax"> About Us </a></li>
                            <li class="subnav">
                                <a href="portfolio" class="ajax"> Projects </a>
                                <ul>
                                    <?php
                                    if (isset($countries) and $countries != false) {
                                        foreach ($countries as $country) {
                                            ?>
                                            <li><a href="<?= base_url('portfolio-cat/' . $country->id . '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', character_limiter($country->country, 50,'')), '-');?>" class="ajax"> Projects in <?= $country->country;?> </a></li>
                                        <?php
                                        }
                                    }
                                    ?>
                                </ul>       
                            </li>
                            <li><a href="enquire" class="ajax"> Enquire Now </a></li>
                            <li><a href="contact" class="ajax"> Contact Us </a></li>
                        </ul>
                    </nav>
                </div>
                <div class="nav-overlay"></div>
                <div class="content full-height no-padding home-content ">
                    <div class="full-height-wrap">
                        <div class="overlay"></div>
                        <div class="slideshow-holder">
                            <div class="slideshow-item owl-carousel">
                                <?php
                                if (isset($sliders)) {
                                    foreach ($sliders as $slider) { ?>
                                        <!-- 1 -->
                                        <div class="item">
                                            <div class="bg" style="background-image:url(<?= $slider->url;?>)"></div>
                                        </div>
                                <?php }
                                }
                                ?>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--=============== scripts  ===============-->
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins.js"></script>
    <script type="text/javascript" src="assets/js/core.js"></script>
    <script type="text/javascript" src="assets/js/scripts.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY" type="text/javascript"></script> 
</body>
</html>