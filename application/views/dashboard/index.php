<!DOCTYPE html>
<html NG-APP="myApp">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nucta - Dashboard </title>


    <link rel="stylesheet" href="assets/dashboard/css/vendor.css" />
    <link rel="stylesheet" href="assets/dashboard/css/app.css" />

</head>
<body ng-controller="HomeController">
    <div id="loading-bar-container"></div>
  <!-- Wrapper-->
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <a data-toggle="dropdown" class="dropdown-toggle">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">Admin</strong>
                            </span> <span class="text-muted text-xs block">Example menu <b class="caret"></b></span>
                        </span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="#profile">Profile</a></li>
                                <li><a href="<?php echo base_url('auth/logout')?>">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            DOT
                        </div>
                    </li>

                    <li >
                        <a href="#/dashboard"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
                    </li>
                    <li><a href="#/slider"><i class="fa fa-picture-o"></i> <span>Slider</span></a></li>
                    <li><a href="#/team"><i class="fa fa-user"></i> <span>Team</span></a></li>
                    <li><a href="#/category"><i class="fa fa-search"></i> <span>Category</span></a></li>
                    <li><a href="#/country"><i class="fa fa-search"></i> <span>Country</span></a></li>
                    <li><a href="#/projects"><i class="fa fa-search"></i> <span>Projects</span></a></li>
                    <li><a href="#/address"><i class="fa fa-address-book"></i> <span>Address</span></a></li>

                </ul>
            </div>
        </nav>


        <!-- Page wraper -->
        <div id="page-wrapper" class="gray-bg">

            <!-- Page wrapper -->
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary "><i class="fa fa-bars"></i> </a>
                        <form role="search" class="navbar-form-custom" method="post" action="/">
                            <div class="form-group">
                                <!--<input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search" />-->
                            </div>
                        </form>
                    </div>
                        <ul class="nav navbar-top-links navbar-right">

                            <!--<li>
                                <a href="#halticketlist" popover-placement="bottom" uib-popover="hallticket" popover-trigger="'mouseenter'"><i class="fa fa-file-text-o"></i></a>
                            </li>
                            <li>
                                <a href="#registration" popover-placement="bottom" uib-popover="list" popover-trigger="'mouseenter'"><i class="fa fa-list-ul"></i></a>
                            </li>
                            <li>
                                <a href="#applicationsearch" popover-placement="bottom" uib-popover="Search" popover-trigger="'mouseenter'"><i class="fa fa-search"></i></a>
                            </li>
                            <li>
                                <a href="#newapplication" popover-placement="bottom" uib-popover="New" popover-trigger="'mouseenter'"><i class="fa fa-upload"></i></a>
                            </li>-->
                            <li>

                                <a href="<?php echo base_url('auth/logout');?>"><i class="fa fa-sign-out"></i> Log out</a>
                            </li>
                        </ul>
                </nav>
            </div>


            <!-- Main view  -->
            <div ng-view></div>

            <!-- Footer -->
            <div class="footer">
                <div class="pull-right">
                    Example text
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2017
                </div>
            </div>


        </div>
        <!-- End page wrapper-->

    </div>
    <!-- End wrapper-->

<script src="assets/dashboard/js/app.js" type="text/javascript"></script>
</body>
</html>
