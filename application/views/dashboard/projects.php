<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row" ng-show="showform">
        <div class="" ng-class="{'col-lg-12' : files.length == 0, 'col-lg-9' : files.length > 0}">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add project</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" ng-submit="addProject()">
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Project Name</label>
                            <div class="col-lg-4"  ng-class="{'has-error' : validationError.name}">
                                <input type="text" placeholder="Project Name" class="form-control" ng-model="newproject.name">
                            </div>

                            <label class="col-lg-2 control-label">Client Name</label>
                            <div class="col-lg-4"  ng-class="{'has-error' : validationError.client_name}">
                                <input type="text" placeholder="Project Name" class="form-control" ng-model="newproject.client_name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Date</label>
                            <div class="col-lg-4"  ng-class="{'has-error' : validationError.about}">
                                <input type="date" class="form-control" ng-model="date">
                            </div>

                            <label class="col-lg-2 control-label">Status</label>
                            <div class="col-lg-4"  ng-class="{'has-error' : validationError.status}">
                                <select  class="form-control" ng-model="newproject.status">
                                    <option value="" selected>Select</option>
                                    <option value="progress">Progress</option>
                                    <option value="completed">Completed</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Country</label>
                            <div class="col-lg-4"  ng-class="{'has-error' : validationError.country_id}">
                                <select  class="form-control" ng-model="newproject.country_id">
                                    <option value="" selected>Select Country</option>
                                    <option value="{{country.id}}" ng-repeat="country in countries">{{country.country}}</option>
                                </select>
                            </div>

                            <label class="col-lg-2 control-label">Category</label>
                            <div class="col-lg-4"  ng-class="{'has-error' : validationError.category_id}">
                                <select  class="form-control" ng-model="newproject.category_id">
                                    <option value="" selected>Select Category</option>
                                    <option value="{{category.id}}" ng-repeat="category in categories">{{category.name}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Description</label>
                            <div class="col-lg-6"  ng-class="{'has-error' : validationError.description}">
                                <textarea class="form-control" ng-model="newproject.description"></textarea>
                            </div>

                        </div>


                        <div class="form-group" ng-class="{'has-error' : validationError.file}">
                            <label for="" class="control-label col-lg-2">Photo</label>
                            <div class="col-md-8">
                                <button ngf-select="uploadFiles($files, $invalidFiles)"
                                        accept="image/*"
                                        ngf-max-height="5000"
                                        ngf-max-size="5MB"
                                        ngf-multiple="true" type="button"
                                        class="upload-drop-zone btn-default"
                                        ngf-drop="uploadFiles($files)"
                                        ngf-drag-over-class="'drop'" ngf-multiple="true"
                                        ngf-pattern="'image/*'"
                                        ng-class="{'upload-drop-zone-error' : validationError.file}">
                                    <div class="dz-default dz-message">
                                        <span><strong>Drop files here or click to upload. </strong></span>
                                    </div>
                                </button>
                                <div class="help-block" ng-show="validationError.file">Please select image!</div>
                                <span class="alert alert-danger" ng-show="fileValidation.status == true">{{fileValidation.msg}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-primary" type="submit" ng-bind="(curproject == false ? 'Add' : 'Update')">Add</button>
                                <button class="btn btn-danger" type="button" ng-click="hideForm()">Cancel</button>
                            </div>
                        </div>
                    </form>

                    <div class="col-lg-5">
                        <div class="lightBoxMoment" ng-show="curproject">
                            <div ng-repeat="file in curproject.files" class="col-md-2">
                                <a class="example-image-link" href="{{file.url}}" data-lightbox="item-list-{{curproject.name}}">
                                    <img src="{{file.url}}" width="50px">
                                </a>
                                <a ng-click="deleteImage(file)" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3" ng-show="files.length > 0">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Upload status</h5>
                </div>
                <div class="ibox-content">
                    <div ng-repeat="file in files">
                        <h5>{{files.name}}</h5>
                        <div class="lightBoxGallery">
                            <a class="example-image-link" href="{{file.$ngfBlobUrl}}" data-lightbox="example-1" data-title="">
                                <img ngf-src="file.$ngfBlobUrl" alt=""  style="width: 25px; max-height: 25px" id="myImg"/>
                            </a>
                        </div>
                        <div class="progress">
                            <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="35" role="progressbar"
                                 class="progress-bar progress-bar-success"
                                 style="width:{{file.progress}}%" ng-show="uploadstatus != 1">
                                <span>{{file.progress}}% Complete</span>
                            </div>
                        </div>
                    </div>
                    <p class="text-danger" ng-repeat="f in errFiles">{{file.name}} {{file.$error}} {{file.$errorParam}}.</p>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Show All projects</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-responsive" datatable="ng" dt-options="dtOptions">
                            <thead>
                            <tr role="row">
                                <th>Sl No</th>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Client</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Country</th>
                                <th>Category</th>
                                <th>Photos</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr role="row" ng-repeat="project in projects">
                                <td>{{$index+1}}</td>
                                <td>{{project.name}}</td>
                                <td>{{project.date}}</td>
                                <td>{{project.client_name}}</td>
                                <td>{{project.description}}</td>
                                <td>{{project.status}}</td>
                                <td>{{project.country.country}}</td>
                                <td>{{project.category.name}}</td>
                                <td class="center">
                                    <a class="example-image-link" href="{{image.url}}" data-lightbox="images-{{project.id}}" data-title="" ng-repeat="image in project.files">
                                        <img src="{{image.url}}" alt=""  style="width: 25px; max-height: 25px"/>
                                    </a>
                                </td>
                                <td class="center">
                                    <div  class="btn-group btn-group-xs" role="group">
                                        <button type="button" class="btn btn-info" ng-click="editProject(project)">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button  type="button" class="btn btn-danger" ng-click="deleteProject(project)">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
