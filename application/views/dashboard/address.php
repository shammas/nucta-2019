<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row" ng-show="showform">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add Address</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" ng-submit="addAddress()">
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Place</label>
                            <div class="col-lg-6"  ng-class="{'has-error' : validationError.place}">
                                <input type="text" placeholder="Place" class="form-control" ng-model="newaddress.place">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Address</label>
                            <div class="col-lg-6"  ng-class="{'has-error' : validationError.address}">
                                <textarea class="form-control" ng-model="newaddress.address"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Phone</label>
                            <div class="col-lg-6"  ng-class="{'has-error' : validationError.phone}">
                                <input type="text" placeholder="Phone" class="form-control" ng-model="newaddress.phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-6"  ng-class="{'has-error' : validationError.email}">
                                <input type="text" placeholder="Email" class="form-control" ng-model="newaddress.email">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-primary" type="submit" ng-bind="(curaddress == false ? 'Add' : 'Update')">Add</button>
                                <button class="btn btn-danger" type="button" ng-click="hideForm()">Cancel</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Show All addresses</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-responsive" datatable="ng" dt-options="dtOptions">
                            <thead>
                            <tr role="row">
                                <th>Sl No</th>
                                <th>Place</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr role="row" ng-repeat="address in addresses">
                                <td>{{$index+1}}</td>
                                <td>{{address.place}}</td>
                                <td>{{address.address}}</td>
                                <td>{{address.phone}}</td>
                                <td>{{address.email}}</td>
                                <td class="center">
                                    <div  class="btn-group btn-group-xs" role="group">
                                        <button type="button" class="btn btn-info" ng-click="editAddress(address)">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <button  type="button" class="btn btn-danger" ng-click="deleteAddress(address)">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
