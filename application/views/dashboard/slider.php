<div class="wrapper wrapper-content animated fadeInRight"  >
    <div class="row" ng-show="showform">
        <div class="" ng-class="{'col-lg-12' : !files, 'col-lg-9' : files}">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add slider</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" ng-submit="addSlider()" >
                        <div class="form-group" ng-class="{'has-error' : validationError.file}">
                            <label for="" class="control-label col-lg-1">Photo</label>
                            <div class="col-md-11">
                                <button ngf-select="uploadFiles($files, $invalidFiles)"
                                        accept="image/*"
                                        ngf-max-height="5000"
                                        ngf-max-size="5MB"
                                        ngf-multiple="true" type="button"
                                        class="upload-drop-zone btn-default"
                                        ngf-drop="uploadFiles($files)"
                                        ngf-drag-over-class="'drop'" ngf-multiple="true"
                                        ngf-pattern="'image/*'"
                                        ng-class="{'upload-drop-zone-error' : validationError.file}">
                                    <div class="dz-default dz-message">
                                        <span><strong>Drop files here or click to upload. </strong></span>
                                    </div>
                                </button>
                                <div class="help-block" ng-show="validationError.file">Please select image!</div>
                                <span class="alert alert-danger" ng-show="fileValidation.status == true">{{fileValidation.msg}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-primary" type="submit" ng-bind="(curslider == false ? 'Add' : 'Update')">Add</button>
                                <button class="btn btn-danger" type="button" ng-click="hideForm()">Cancel</button>
                            </div>
                        </div>
                    </form>
                    <div class="lightBoxGallery" ng-show="curslider">
                        <a class="example-image-link" href="{{curslider.url}}" data-lightbox="item-list" data-title="">
                            <img src="{{curslider.url}}" width="100px">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3" ng-show="files.length > 0">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Upload status</h5>
                </div>
                <div class="ibox-content">
                    <div ng-repeat="file in files">
                        <h5>{{files.name}}</h5>
                        <div class="lightBoxGallery">
                            <a class="example-image-link" href="{{file.$ngfBlobUrl}}" data-lightbox="example-1" data-title="">
                                <img ngf-src="file.$ngfBlobUrl" alt=""  style="width: 25px; max-height: 25px" id="myImg"/>
                            </a>
                        </div>
                        <div class="progress">
                            <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="35" role="progressbar"
                                 class="progress-bar progress-bar-success"
                                 style="width:{{file.progress}}%" ng-show="uploadstatus != 1">
                                <span>{{file.progress}}% Complete</span>
                            </div>
                        </div>
                    </div>
                    <p class="text-danger" ng-repeat="f in errFiles">{{file.name}} {{file.$error}} {{file.$errorParam}}.</p>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Show All sliders</h5>
                    <button type="button" class="btn btn-primary btn-xs" ng-click="newSlider()">Add Slider</button>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12" ng-repeat="slide in sliders">
                            <div class="hovereffect">
                                <img class="img-responsive" ng-src="{{slide.url}}" alt="">
                                <div class="overlay">
                                    <a class="info" ng-click="deleteSlider(slide)">Delete</a>
                                    <a class="info" href="{{slide.url}}" data-lightbox="item-list" data-title="">view</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
