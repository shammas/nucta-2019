<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="index, follow"/>
    <meta property="og:title" content="NUCTA is an inception for architectural and interior design solutions" />
    <meta property="og:site_name" content="NUCTA Architectural and Interior design solutions" />
    <meta property="og:url" content="http://nuctaweb.com/" />
    <meta property="og:description" content="NUCTA is an inception for architectural and interior design solutions started in 2016, by a team of three interior designers zealous about architecture, striving to fulfill your demands and dreams. It also consists of a young team of managers, supervisors and visualizers. The firm also handles furniture design and landscaping works." />
    <meta property="og:type" content="website" />
    <title>Nucta | architecture + interiors</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="NUCTA is an inception for architectural and interior design solutions started in 2016, by a team of three interior designers zealous about architecture, striving to fulfill your demands and dreams. It also consists of a young team of managers, supervisors and visualizers. The firm also handles furniture design and landscaping works.">
    <meta name="keywords" content="Nucta wandoor, Nucta Calicut, Nucta Qatar, Builders in Wandoor, Builders in Calicut, Builders in Qatar, Builders in Malappuram, Builders in Kearala, ready to occupy villas in Wandoor, ready to occupy villas in Calicut, ready to occupy villas in Qatar, ready to occupy villas in Kearala, budget interiors in Wandoor, budget interiors in Calicut, budget interiors in Qatar, budget interiors in Kearala" />
    <link rel="shortcut icon" href="assets/images/favicon.jpg">
    <!--=============== css  ===============--> 
    <link type="text/css" rel="stylesheet" href="assets/css/reset.css">
    <link type="text/css" rel="stylesheet" href="assets/css/plugins.css">
    <link type="text/css" rel="stylesheet" href="assets/css/style.css">
    <link type="text/css" rel="stylesheet" href="assets/css/yourstyle.css">
</head>
<body>
    <div class="loader">
        <div class="tm-loader">
            <div id="circle"></div>
        </div>
    </div>
    <div id="main">
        <header>
            <div class="nav-button">
                <span  class="nos"></span>
                <span class="ncs"></span>
                <span class="nbs"></span>
            </div>
            <div class="logo-holder">
                <a href="/" class="ajax"><img src="assets/images/logo.png" alt=""></a>
            </div>
            <div class="header-title">
                <h2><a class="ajax" href="#"></a></h2>
            </div>			
        </header>
        <div id="wrapper">
            <div class="content-holder elem scale-bg2 transition3">
                <div class="dynamic-title">Our Works</div>
                <div class="nav-overlay"></div>
                <div class="nav-inner isDown">
                    <nav>
                        <ul>
                            <li><a href="/" class="ajax"> Home </a></li>
                            <li><a href="about" class="ajax"> About Us </a></li>
                            <li class="subnav">
                                <a href="portfolio" class="ajax active"> Projects </a>
                                <ul>
                                    <?php
                                    if (isset($countries) and $countries != false) {
                                        foreach ($countries as $country) {
                                            ?>
                                            <li><a href="<?= base_url('portfolio-cat/' . $country->id . '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', character_limiter($country->country, 50,'')), '-');?>" class="ajax"> Projects in <?= $country->country;?> </a></li>
                                        <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li>
                            <li><a href="enquire" class="ajax"> Enquire Now </a></li>
                            <li><a href="contact" class="ajax"> Contact Us </a></li>
                        </ul>
                    </nav>
                </div>
                <!--=============== Content ===============-->
                <div class="content ">
                    <section class="no-padding no-border no-bg">
                        <div class="fixed-info-container">
                            <div class="filter-holder filter-vis-column">
                                <div class="gallery-filters at">
                                    <a href="#" class="gallery-filter filall gallery-filter-active"  data-filter="*">All</a>
                                    <?php
                                        if (isset($countries) and $countries != false) {
                                            foreach ($countries as $country) { ?>
                                                <div class="nation-down">
                                                    <p><?= $country->country;?></p>
                                                    <div class="dropdown-content">
                                                    <?php
                                                        if (isset($categories) and $categories != false) {
                                                            foreach ($categories as $category) { ?>
                                                                <a href="#" class="gallery-filter" data-filter=".<?= $country->country . '-' . $category->name; ?>"><?= ucfirst($category->name);?></a>
                                                            <?php }
                                                                ?>
                                                            <?php
                                                            }
                                                        ?>
                                                    </div>
                                                </div>
                                        <?php }
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="resize-carousel-holder vis-info">
                            <div class="gallery-items    hid-port-info grid-small-pad">
                                <?php
                                if (isset($projects) and $projects != false) {
                                    foreach ($projects as $project) {

                                        if (isset($project->files) && $project->files != null) {
                                            $project->files = (array)$project->files;
                                        }
                                        ?>
                                        <!-- single portfolio -->
                                        <div class="gallery-item <?= $project->country->country . '-' . $project->category->name;?>">
                                            <div class="grid-item-holder">
                                                <div class="box-item">
                                                    <?php
                                                    if ($project->files != null and is_object(current($project->files))) {
                                                        echo '<img src="' . current($project->files)->url . '" alt="">';
                                                    }else{
                                                        echo '<img  src="'.base_url().'assets/images/portfolio.jpg" alt="">';
                                                    }?>
                                                </div>
                                                <div class="grid-item">
                                                    <h3><a href="<?= base_url('portfolio/' . $project->id . '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', character_limiter($project->name, 50,'')), '-');?>" class="ajax portfolio-link"><?= character_limiter($project->name,25);?></a></h3>
                                                    <span><?= $project->category->name;?></span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                    }
                                }
                                ?>

                            </div>    
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <footer>
            <div class="policy-box">
                <span>&#169; NUCTA 2019  /  Designed by <a href="http://cloudbery.com/" target="_blank"><img src="assets/images/cloudbery.png" alt="Cloudbery Solutions"></a></span>
            </div>
            <div class="footer-social">
                <ul>
                    <li><a href="https://www.facebook.com/Nuctaac/" target="_blank" ><i class="fa fa-facebook"></i><span>facebook</span></a></li>
                    <li><a href="https://www.instagram.com/nucta_architecture/" target="_blank" ><i class="fa fa-instagram"></i><span>instagram</span></a></li>
                    <!-- <li><a href="#" target="_blank"><i class="fa fa-twitter"></i><span>twitter</span></a></li>
                    <li><a href="#" target="_blank" ><i class="fa fa-pinterest"></i><span>pinterest</span></a></li>
                    <li><a href="#" target="_blank" ><i class="fa fa-tumblr"></i><span>tumblr</span></a></li> -->
                </ul>
            </div>
        </footer>
    </div>
    <!-- Main end -->
    <!--=============== scripts  ===============-->
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins.js"></script>
    <script type="text/javascript" src="assets/js/core.js"></script>
    <script type="text/javascript" src="assets/js/scripts.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY" type="text/javascript"></script>
</body>
</html>