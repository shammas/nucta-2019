<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="index, follow"/>
    <meta property="og:title" content="NUCTA is an inception for architectural and interior design solutions" />
    <meta property="og:site_name" content="NUCTA Architectural and Interior design solutions" />
    <meta property="og:url" content="http://nuctaweb.com/" />
    <meta property="og:description" content="NUCTA is an inception for architectural and interior design solutions started in 2016, by a team of three interior designers zealous about architecture, striving to fulfill your demands and dreams. It also consists of a young team of managers, supervisors and visualizers. The firm also handles furniture design and landscaping works." />
    <meta property="og:type" content="website" />
    <title>Nucta | architecture + interiors</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="NUCTA is an inception for architectural and interior design solutions started in 2016, by a team of three interior designers zealous about architecture, striving to fulfill your demands and dreams. It also consists of a young team of managers, supervisors and visualizers. The firm also handles furniture design and landscaping works.">
    <meta name="keywords" content="Nucta wandoor, Nucta Calicut, Nucta Qatar, Builders in Wandoor, Builders in Calicut, Builders in Qatar, Builders in Malappuram, Builders in Kearala, ready to occupy villas in Wandoor, ready to occupy villas in Calicut, ready to occupy villas in Qatar, ready to occupy villas in Kearala, budget interiors in Wandoor, budget interiors in Calicut, budget interiors in Qatar, budget interiors in Kearala" />
    <link rel="shortcut icon" href="<?= base_url()?>assets/images/favicon.jpg">
    <!--=============== css  ===============--> 
    <link type="text/css" rel="stylesheet" href="<?= base_url();?>assets/css/reset.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url();?>assets/css/plugins.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url();?>assets/css/style.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url();?>assets/css/yourstyle.css">
</head>
<body>
    <div class="loader">
        <div class="tm-loader">
            <div id="circle"></div>
        </div>
    </div>
    <div id="main">
        <header>
            <div class="nav-button">
                <span  class="nos"></span>
                <span class="ncs"></span>
                <span class="nbs"></span>
            </div>
            <div class="logo-holder">
                <a href="<?= base_url();?>" class="ajax"><img src="<?= base_url()?>assets/images/logo.png" alt=""></a>
            </div>
            <div class="header-title">
                <h2><a class="ajax" href="#"></a></h2>
            </div>          
        </header>
        <div id="wrapper">
            <div class="content-holder elem scale-bg2 transition3">
                <div class="dynamic-title">Our Works in India</div>
                <div class="nav-overlay"></div>
                <div class="nav-inner isDown">
                    <nav>
                        <ul>
                            <li><a href="/" class="ajax"> Home </a></li>
                            <li><a href="about" class="ajax"> About Us </a></li>
                            <li class="subnav">
                                <a href="portfolio" class="ajax active"> Projects </a>
                                <ul>
                                    <?php
                                    if (isset($countries) and $countries != false) {
                                        foreach ($countries as $country) {
                                            ?>
                                            <li><a href="<?= base_url('portfolio-cat/' . $country->id . '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', character_limiter($country->country, 50,'')), '-');?>" class="ajax"> Projects in <?= $country->country;?> </a></li>
                                        <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </li>
                            <li><a href="enquire" class="ajax"> Enquire Now </a></li>
                            <li><a href="contact" class="ajax"> Contact Us </a></li>
                        </ul>
                    </nav>
                </div>
                <!--=============== Content ===============-->
                <div id="potsingle" class="content full-height no-padding">
                    <div class="fixed-info-container">
                        <div class="content-nav">
                            <ul>
                                <li>
                                    <?php if ($previous) { ?>
                                        <a href="<?= base_url('portfolio/' . $previous->id . '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', character_limiter($previous->name, 50,'')), '-');?>" class="ajax ln"><i class="fa fa fa-angle-left"></i></a>
                                    <?php }else{ ?>
                                        <a href="" class="ajax ln"><i class="fa fa fa-angle-left"></i></a>
                                    <?php }?>

                                </li>
                                <li>
                                    <div class="list">
                                        <a href="<?= base_url('portfolio')?>" class="ajax">
                                        <span>
                                        <i class="b1 c1"></i><i class="b1 c2"></i><i class="b1 c3"></i>
                                        <i class="b2 c1"></i><i class="b2 c2"></i><i class="b2 c3"></i>
                                        <i class="b3 c1"></i><i class="b3 c2"></i><i class="b3 c3"></i>
                                        </span></a>
                                    </div>
                                </li>
                                <li>
                                    <?php if ($next) { ?>
                                        <a href="<?= base_url('portfolio/' . $next->id . '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', character_limiter($next->name, 50,'')), '-');?>" class="ajax rn"><i class="fa fa fa-angle-right"></i></a>
                                    <?php }else{ ?>
                                        <a href="" class="ajax rn"><i class="fa fa fa-angle-right"></i></a>
                                    <?php }?>
                                    </li>
                            </ul>
                        </div>                           
                        <h3><?= (isset($project) ? $project->name : '');?></h3>
                        <div class="separator"></div>
                        <div class="clearfix"></div>
                        <p><?= (isset($project) ? $project->description : '');?>.</p>
                        <h4>Info</h4>
                        <ul class="project-details">
                            <li><span>Date :</span> <?= date('d.m.Y', strtotime((isset($project) ? $project->date : '')));?> </li>
                            <li><span>Client :</span>  <?= ucfirst((isset($project) ? $project->client_name : ''));?></li>
                            <li><span>Status :</span> <?= ucfirst((isset($project) ? $project->status : ''));?> </li>
                            <li><span>Location : </span>  <?= (isset($project) ? $project->location : '');?></li>
<!--                            <li><span>Location : </span>  <a href="https://goo.gl/maps/UzN5m" target="_blank"> Kharkiv Ukraine  </a></li>-->
                        </ul>
                    </div>
                    <div class="resize-carousel-holder vis-info gallery-horizontal-holder">
                        <div id="gallery_horizontal" class="gallery_horizontal owl_carousel">
                            <?php
                            if (isset($project) and $project->files != null) {
                                foreach ($project->files as $image) {
                                    ?>
                                    <!-- gallery Item-->
                                    <div class="horizontal_item">
                                        <div class="zoomimage"><img src="<?= $image->url;?>"
                                                                    class="intense" alt=""><i class="fa fa-expand"></i>
                                        </div>
                                        <img src="<?= $image->url;?>" alt="">
                                    </div>
                                    <!-- gallery Item-->
                                <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="customNavigation">
                            <a class="prev-slide transition"><i class="fa fa-angle-left"></i></a>
                            <a class="next-slide transition"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="policy-box">
                <span>&#169; NUCTA 2019  /  Designed by <a href="http://cloudbery.com/" target="_blank"><img src="<?= base_url()?>assets/images/cloudbery.png" alt="Cloudbery Solutions"></a></span>
            </div>
            <div class="footer-social">
                <ul>
                    <li><a href="https://www.facebook.com/Nuctaac/" target="_blank" ><i class="fa fa-facebook"></i><span>facebook</span></a></li>
                    <li><a href="https://www.instagram.com/nucta_architecture/" target="_blank" ><i class="fa fa-instagram"></i><span>instagram</span></a></li>
                    <!-- <li><a href="#" target="_blank"><i class="fa fa-twitter"></i><span>twitter</span></a></li>
                    <li><a href="#" target="_blank" ><i class="fa fa-pinterest"></i><span>pinterest</span></a></li>
                    <li><a href="#" target="_blank" ><i class="fa fa-tumblr"></i><span>tumblr</span></a></li> -->
                </ul>
            </div>
        </footer>
    </div>
    <!-- Main end -->
    <!--=============== scripts  ===============-->
    <script type="text/javascript" src="<?= base_url()?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>assets/js/plugins.js"></script>
    <script type="text/javascript" src="<?= base_url()?>assets/js/core.js"></script>
    <script type="text/javascript" src="<?= base_url()?>assets/js/scripts.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY" type="text/javascript"></script>
</body>
</html>