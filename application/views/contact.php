<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="index, follow"/>
    <meta property="og:title" content="NUCTA is an inception for architectural and interior design solutions" />
    <meta property="og:site_name" content="NUCTA Architectural and Interior design solutions" />
    <meta property="og:url" content="http://nuctaweb.com/" />
    <meta property="og:description" content="NUCTA is an inception for architectural and interior design solutions started in 2016, by a team of three interior designers zealous about architecture, striving to fulfill your demands and dreams. It also consists of a young team of managers, supervisors and visualizers. The firm also handles furniture design and landscaping works." />
    <meta property="og:type" content="website" />
    <title>Nucta | architecture + interiors</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="NUCTA is an inception for architectural and interior design solutions started in 2016, by a team of three interior designers zealous about architecture, striving to fulfill your demands and dreams. It also consists of a young team of managers, supervisors and visualizers. The firm also handles furniture design and landscaping works.">
    <meta name="keywords" content="Nucta wandoor, Nucta Calicut, Nucta Qatar, Builders in Wandoor, Builders in Calicut, Builders in Qatar, Builders in Malappuram, Builders in Kearala, ready to occupy villas in Wandoor, ready to occupy villas in Calicut, ready to occupy villas in Qatar, ready to occupy villas in Kearala, budget interiors in Wandoor, budget interiors in Calicut, budget interiors in Qatar, budget interiors in Kearala" />
    <link rel="shortcut icon" href="assets/images/favicon.jpg">
    <!--=============== css  ===============-->
    <link type="text/css" rel="stylesheet" href="assets/css/reset.css">
    <link type="text/css" rel="stylesheet" href="assets/css/plugins.css">
    <link type="text/css" rel="stylesheet" href="assets/css/style.css">
    <link type="text/css" rel="stylesheet" href="assets/css/yourstyle.css">
</head>
<body>
<div class="loader">
    <div class="tm-loader">
        <div id="circle"></div>
    </div>
</div>
<div id="main">
    <header>
        <div class="nav-button">
            <span  class="nos"></span>
            <span class="ncs"></span>
            <span class="nbs"></span>
        </div>
        <div class="logo-holder">
            <a href="/" class="ajax"><img src="assets/images/logo.png" alt=""></a>
        </div>
        <div class="header-title">
            <h2><a class="ajax" href="#"></a></h2>
        </div>
    </header>
    <div id="wrapper">
        <div class="content-holder elem scale-bg2 transition3">
            <div class="dynamic-title">Contact Us</div>
            <div class="nav-overlay"></div>
            <div class="nav-inner isDown">
                <nav>
                    <ul>
                        <li><a href="/" class="ajax"> Home </a></li>
                        <li><a href="about" class="ajax"> About Us </a></li>
                        <li class="subnav">
                            <a href="portfolio" class="ajax"> Projects </a>
                            <ul>
                                <?php
                                if (isset($countries) and $countries != false) {
                                    foreach ($countries as $country) {
                                        ?>
                                        <li><a href="<?= base_url('portfolio-cat/' . $country->id . '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', character_limiter($country->country, 50,'')), '-');?>" class="ajax"> Projects in <?= $country->country;?> </a></li>
                                    <?php
                                    }
                                }
                                ?>
                            </ul>
                        </li>
                        <li><a href="enquire" class="ajax"> Enquire Now </a></li>
                        <li><a href="contact" class="ajax active"> Contact Us </a></li>
                    </ul>
                </nav>
            </div>
            <!--=============== Content ===============-->
            <div class="content full-height">
                <div class="wrapper-inner">
                    <div class="align-content">
                        <section>
                            <div class="container small-container" style="margin-top: 10%;margin-bottom: 10%;">
                                <h3 class="dec-text">Contact us</h3>
                                <?php
                                if (isset($addresses) and $addresses != false) {
                                    foreach ($addresses as $address) {
                                        ?>
                                        <ul class="contact-list">
                                            <li class="map-location"><?= $address->place;?></li>
                                            <li><span>Address </span>
                                                <a href="#"><?= $address->address;?></a>
                                            </li>
                                            <li><span>Phone</span>
                                                <a href="#"><?= $address->phone;?></a>
                                            </li>
                                            <li>
                                                <span>E-mail </span>
                                                <a href="#"><?= $address->email;?></a>
                                            </li>
                                        </ul>
                                    <?php
                                    }
                                }
                                ?>
                                <a href="#" class=" btn anim-button   trans-btn   transition  fl-l showform"><span>Write us</span><i class="fa fa-eye"></i></a>
                            </div>
                        </section>
                    </div>
                    <div class="contact-form-holder">
                        <div class="close-contact"></div>
                        <div class="align-content">
                            <section>
                                <div id="contact-form">
                                    <div id="message"></div>
                                    <form method="post" action="<?= base_url()?>index/contactUs" name="contactform" id="contactform">
                                        <input name="name" type="text" id="name"  onClick="this.select()" value="Name" >
                                        <input name="email" type="text" id="email" onClick="this.select()" value="E-mail" >
                                        <input type="text"  name="phone" id="phone" onClick="this.select()" value="Phone" />
                                        <textarea name="subject"  id="comments" onClick="this.select()" >Message</textarea>
<!--                                        <div class="verify-wrap">-->
<!--                                            <span class="verify-text"> How many gnomes were in the story about the "Snow-white" ?</span>-->
<!--                                            <input name="verify" type="text" id="verify" onClick="this.select()" value="0" >-->
<!--                                        </div>-->
                                        <button type="submit"  id="submit"><span>Send </span> <i class="fa fa-long-arrow-right"></i></button>
                                    </form>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="fixed-column">
                    <div class="map-box">
                        <!-- <div  id="map-canvas"></div> -->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62630.16011760837!2d76.13976647735026!3d11.159103167925903!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba63065cf6800c9%3A0x7fc826bfa2e9fa45!2sN+U+C+T+A!5e0!3m2!1sen!2sin!4v1551169696638" width="700" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="policy-box">
            <span>&#169; NUCTA 2019  /  Designed by <a href="http://cloudbery.com/" target="_blank"><img src="assets/images/cloudbery.png" alt="Cloudbery Solutions"></a></span>
        </div>
        <div class="footer-social">
            <ul>
                <li><a href="https://www.facebook.com/Nuctaac/" target="_blank" ><i class="fa fa-facebook"></i><span>facebook</span></a></li>
                <li><a href="https://www.instagram.com/nucta_architecture/" target="_blank" ><i class="fa fa-instagram"></i><span>instagram</span></a></li>
                <!-- <li><a href="#" target="_blank"><i class="fa fa-twitter"></i><span>twitter</span></a></li>
                <li><a href="#" target="_blank" ><i class="fa fa-pinterest"></i><span>pinterest</span></a></li>
                <li><a href="#" target="_blank" ><i class="fa fa-tumblr"></i><span>tumblr</span></a></li> -->
            </ul>
        </div>
    </footer>


</div>
<!-- Main end -->
<!--=============== google map ===============-->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY" type="text/javascript"></script>
<!--=============== scripts  ===============-->
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/plugins.js"></script>
<script type="text/javascript" src="assets/js/core.js"></script>
<script type="text/javascript" src="assets/js/scripts.js"></script>
</body>
</html>