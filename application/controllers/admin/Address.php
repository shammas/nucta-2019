<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends CI_Controller
{

    //        public $delete_cache_on_save = TRUE;
    function __construct()
    {
        parent::__construct();
        $this->load->model('Address_model', 'address');

        $this->load->library(['image_lib','ion_auth']);

        $this->load->library('form_validation');
        $this->load->helper('url');

        if (!$this->ion_auth->logged_in()) {
            $data['error'] = 'Authentication Failed';
            $this->output->set_status_header(200, 'Unauthenticated');
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
            die();
        }
    }

    function index()
    {
        $data = $this->address->as_array()->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }

    function get_all()
    {
        $data = $this->address->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    function store()
    {
        $this->form_validation->set_rules('address', 'Address', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            if ($this->address->insert($this->input->post())) {
                $this->output->set_content_type('application/json')->set_output(json_encode($this->input->post()));
            } else {
                $this->output->set_status_header(400, 'Server Error');
                $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Something went wrong']));
            }
        }
    }


    function update($id)
    {
        $this->form_validation->set_rules('address', 'Address', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            unset($post_data['id']);
            if($this->address->update($post_data,$id)) {
                $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
            }else {
                $this->output->set_status_header(500, 'Server Down');
                $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Server Down.']));
            }
        }
    }

    public function delete($id)
    {
        $address = $this->address->where('id', $id)->get();
        if ($address) {
            $this->address->delete($id);
            $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Address Deleted']));
        } else {
            $this->output->set_status_header(500, 'Validation error');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'The Record Not found']));
        }
    }

}
