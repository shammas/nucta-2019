<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller
{

    //        public $delete_cache_on_save = TRUE;
    function __construct()
    {
        parent::__construct();
        $this->load->model('project_model', 'project');
        $this->load->model('Project_file_model', 'project_file');
        $this->load->library(['upload', 'image_lib', 'ion_auth', 'form_validation']);
        $this->load->helper('url');

        if (!$this->ion_auth->logged_in()) {
            $data['error'] = 'Authentication Failed';
            $this->output->set_status_header(200, 'Unauthenticated');
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
            die();
        }
    }

    function index()
    {
        $data = $this->project->with_files()->with_category()->with_country()->as_array()->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    function get_all()
    {
        $data = $this->project->with_files()->with_category()->with_country()->as_array()->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    function store()
    {
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('client_name', 'Client Name', 'required');
        $this->form_validation->set_rules('country_id', 'Country', 'required');
        $this->form_validation->set_rules('category_id', 'Country', 'required');

        if ($this->form_validation->run() === FALSE || $this->input->post('uploaded') == null) {
            $error = $this->form_validation->get_errors();
            if($this->input->post('uploaded') == null)
                $error['file'] = 'Select a image';

            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($error));

        } else {
            $post_data = $this->input->post();
            $uploaded = json_decode($post_data['uploaded']);
            unset($post_data['uploaded']);

            if (!empty($uploaded)) {

                $project_id = $this->project->insert($post_data);
                if ($project_id) {
                    foreach ($uploaded as $upload) {
                        /*INSERT FILE DATA TO DB*/
                        $file_data['project_id'] = $project_id;
                        $file_data['file_name'] = $upload->file_name;
                        $file_data['url'] = base_url() . 'uploads/project/' . $upload->file_name;
                        $file_data['path'] = $upload->full_path;

                        $file_id = $this->project_file->insert($file_data);

                        if ($file_id) {
                            $thumbDir = getcwd() . '/uploads/project/thumb';

                            if (!is_dir($thumbDir)) {
                                mkdir($thumbDir, 0777, TRUE);
                            }

                            /*****Create Thumb Image****/
                            $img_cfg['source_image'] = $upload->full_path;
                            $img_cfg['maintain_ratio'] = TRUE;
                            $img_cfg['new_image'] = $thumbDir . $upload->file_name;
                            $img_cfg['quality'] = 99;
                            $img_cfg['master_dim'] = 'height';
                            $img_cfg['height'] = 50;

                            $resize_error = [];
                            $this->image_lib->initialize($img_cfg);
                            if (!$this->image_lib->resize()) {
                                $resize_error[] = $this->image_lib->display_errors();
                            }
                            $this->image_lib->clear();

                            /********End Thumb*********/

                            /*resize and create thumbnail image*/
                            if ($upload->file_size > 1024) {
                                $img_cfg['image_library'] = 'gd2';
                                $img_cfg['source_image'] = $upload->full_path;
                                $img_cfg['maintain_ratio'] = TRUE;
                                $img_cfg['new_image'] = $upload->full_path;
                                $img_cfg['height'] = 500;
                                $img_cfg['quality'] = 100;
                                $img_cfg['master_dim'] = 'height';

                                $this->image_lib->initialize($img_cfg);
                                if (!$this->image_lib->resize()) {
                                    $resize_error[] = $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();

                                /********End resize*********/
                            }
                        }
                    }

                    $this->output->set_content_type('application/json')->set_output(json_encode($this->project->where('id', $project_id)->with_files()->get()));
                }else{
                    $this->output->set_status_header(402, 'Server Down');
                    $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'try again later']));

                }
            }else{
                $this->output->set_status_header(400, 'Validation Error');
                $this->output->set_content_type('application/json')->set_output(json_encode(['file' => 'Select any file']));
            }
        }
    }

    function update($id){
        $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            $uploaded = json_decode($post_data['uploaded']);

            unset($post_data['uploaded']);
            unset($post_data['files']);
            unset($post_data['category']);
            unset($post_data['country']);
            unset($post_data['id']);
            if ($this->project->update($post_data,$id)) {
                if (!empty($uploaded)) {
                    foreach ($uploaded as $upload) {
                        /*INSERT FILE DATA TO DB*/
                        $file_data['project_id'] = $id;
                        $file_data['file_name'] = $upload->file_name;
                        $file_data['url'] = base_url() . 'uploads/project/' . $upload->file_name;
                        $file_data['path'] = $upload->full_path;

                        $file_id = $this->project_file->insert($file_data);
                        if ($file_id) {
                            /*****Create Thumb Image****/
                            $img_cfg['source_image'] = $upload->full_path;
                            $img_cfg['maintain_ratio'] = TRUE;
                            $img_cfg['new_image'] = getcwd() . '/uploads/project/thumb/' . $upload->file_name;
                            $img_cfg['quality'] = 99;
                            $img_cfg['height'] = 50;
                            $img_cfg['master_dim'] = 'height';

                            $this->image_lib->initialize($img_cfg);
                            if (!$this->image_lib->resize()) {
                                $resize_error[] = $this->image_lib->display_errors();
                            }
                            $this->image_lib->clear();

                            /********End Thumb*********/

                            /*resize and create thumbnail image*/
                            if ($upload->file_size > 1024) {
                                $img_cfg['image_library'] = 'gd2';
                                $img_cfg['source_image'] = $upload->full_path;
                                $img_cfg['maintain_ratio'] = TRUE;
                                $img_cfg['new_image'] = getcwd() . '/uploads/project/' . $upload->file_name;
                                $img_cfg['height'] = 500;
                                $img_cfg['quality'] = 100;
                                $img_cfg['master_dim'] = 'height';

                                $this->image_lib->initialize($img_cfg);
                                if (!$this->image_lib->resize()) {
                                    $resize_error[] = $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();

                                /********End resize*********/
                            }
                        } else {
                            log_massage('debug', 'update files failed on project update');
                            $this->output->set_status_header(500, 'Server Down');
                            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Try again later.']));
                        }
                    }

                }

                $this->output->set_content_type('application/json')->set_output(json_encode($this->project->where('id', $this->input->post('id'))->with_files()->as_array()->get_all()));
            }else{
                log_massage('debug', 'update failed on project');
                $this->output->set_status_header(500, 'Server Down');
                $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Try again later.']));
            }
        }
    }

    function delete_image($id)
    {
        $project = $this->project_file->where('id', $id)->get();
        if ($project != false) {
            if (file_exists($project->path)) {
                unlink($project->path);
            }
            $this->project_file->delete($id);
            $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Image Delete']));
        }else{
            $this->output->set_status_header(400, 'Server Down');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Try again later']));
        }
    }

    function upload()
    {
        if (!is_dir('uploads/project')) {
            mkdir('./uploads/project', 0777, TRUE);
        }
        try {
            $config['upload_path'] = getcwd() . '/uploads/project';
            $config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';
            $config['max_size'] = 4096;
            $config['file_name'] = date('YmdHis');

            $this->upload->initialize($config);

            if ($this->upload->do_upload('file')) {
                $this->output->set_content_type('application/json')->set_output(json_encode($this->upload->data()));
            } else {
                $this->output->set_status_header(401, 'File Upload Error');
                $this->output->set_output($this->upload->display_errors());
            }
        } catch (Exception $e){
            return false;
        }
    }

    public function delete($id)
    {
        $project = $this->project->where('id', $id)->get();

        if ($project) {
            $project_files = $this->project_file->where('project_id', $id)->get_all();
            if ($project_files) {
                foreach ($project_files as $file) {
                    if ($this->project_file->delete($file->id)) {
                        if(file_exists($file->path)) {
                            unlink($file->path);
                        }
                        $status = 1;
                    } else {
                        $status = 0;
                    }
                }
                if ($status == 1) {
                    if ($this->project->delete($id)) {
                        $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'project Deleted']));
                    }
                } elseif ($status == 0) {
                    $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'project not deleted but some files are deleted']));
                }
            }

            if ($this->project->delete($id)) {
                $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'project Deleted']));
            }
        } else {
            $this->output->set_status_header(500, 'Server Down');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'The Record Not found']));
        }
    }
}