<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends CI_Controller
{

    //        public $delete_cache_on_save = TRUE;
    function __construct()
    {
        parent::__construct();
        $this->load->model('Team_model', 'team');

        $this->load->library(['upload', 'image_lib', 'ion_auth', 'form_validation']);


        if (!$this->ion_auth->logged_in()) {
            $data['error'] = 'Authentication Failed';
            $this->output->set_status_header(200, 'Unauthenticated');
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
            die();
        }
    }

    function index()
    {
        $data = $this->team->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }

    function get_all()
    {
        $data = $this->team->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    function store()
    {

        $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            $uploaded = json_decode($post_data['uploaded']);

            unset($post_data['uploaded']);

            if (!empty($uploaded)) {
                /*INSERT FILE DATA TO DB*/
                $post_data['file_name'] = $uploaded->file_name;
                $post_data['url'] = base_url() . 'uploads/team/' . $uploaded->file_name;
                $post_data['path'] = $uploaded->full_path;

                if ($this->team->insert($post_data)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                }else{
                    $this->output->set_status_header(400, 'Server Down');
                    $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'try again later']));
                }
            } else {
                if ($this->team->insert($post_data)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                }
            }
        }
    }

    function update($id){
        $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            $uploaded = json_decode($post_data['uploaded']);
            unset($post_data['uploaded']);
            unset($post_data['id']);

            if (!empty($uploaded)) {
                /*INSERT FILE DATA TO DB*/
                $post_data['file_name'] = $uploaded->file_name;
                $post_data['url'] = base_url() . 'uploads/team/' . $uploaded->file_name;
                $post_data['path'] = $uploaded->full_path;

                if ($this->team->update($post_data,$id)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                }else{
                    $this->output->set_status_header(400, 'Server Down');
                    $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'try again later']));
                }
            } else {
                if ($this->team->update($post_data,$id)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                }
            }


        }
    }


    function upload()
    {
        if (!is_dir('uploads/team')) {
            mkdir('./uploads/team', 0777, TRUE);
        }
        try {
            $config['upload_path'] = getcwd() . '/uploads/team';
            $config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';
            $config['max_size'] = 4096;
            $config['file_name'] = date('YmdHis');

            $this->upload->initialize($config);

            if ($this->upload->do_upload('file')) {
                $this->output->set_content_type('application/json')->set_output(json_encode($this->upload->data()));
            } else {
                $this->output->set_status_header(401, 'File Upload Error');
                $this->output->set_output($this->upload->display_errors());
            }
        } catch (Exception $e){
            return false;
        }
    }



    public function delete($id)
    {
        $team = $this->team->where('id', $id)->get();
        if ($team) {
            if (file_exists($team->path)) {
                unlink($team->path);
                if ($this->team->delete($id)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Team Deleted']));
                } else {
                    $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Team not deleted but some files are deleted']));
                }
            } else {
                $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Team file not exist in directory']));
            }
        } else {
            $this->output->set_status_header(500, 'Validation error');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'The Record Not found']));
        }
    }


}