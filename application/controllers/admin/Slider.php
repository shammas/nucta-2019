<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller
{

    //        public $delete_cache_on_save = TRUE;
    function __construct()
    {
        parent::__construct();
        $this->load->model('Slider_model', 'slider');

        $this->load->library(['upload', 'image_lib', 'ion_auth', 'form_validation']);


        if (!$this->ion_auth->logged_in()) {
            $data['error'] = 'Authentication Failed';
            $this->output->set_status_header(200, 'Unauthenticated');
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
            die();
        }
    }

    function index()
    {
        $data = $this->slider->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }

    function get_all()
    {
        $data = $this->slider->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    function store()
    {
        if ($this->input->post('uploaded') == null) {
            $error = $this->form_validation->get_errors();
            if ($this->input->post('uploaded') == null)
                $error['file'] = 'Select a image';
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($error));
        }else{

            $uploaded = json_decode($this->input->post('uploaded'));

            if (!empty($uploaded)) {
                foreach ($uploaded as $upload) {
                    /*INSERT FILE DATA TO DB*/
                    $post_data['file_name'] = $upload->file_name;
                    $post_data['url'] = base_url() . 'uploads/slider/' . $upload->file_name;
                    $post_data['path'] = $upload->full_path;
                    try{
                        $this->slider->insert($post_data);
                    }catch(Exception $e){
                        $this->output->set_status_header(400, 'Server Down');
                        $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'try again later']));
                        return false;
                    }
                }
                $this->output->set_content_type('application/json')->set_output(json_encode($uploaded));

            } else {
                $error['file'] = 'Select a image';
                $this->output->set_status_header(400, 'Validation Error');
                $this->output->set_content_type('application/json')->set_output(json_encode($error));
            }
        }
    }

    function update($id){

    }


    function upload()
    {
        if (!is_dir('uploads/slider')) {
            mkdir('./uploads/slider', 0777, TRUE);
        }
        try {
            $config['upload_path'] = getcwd() . '/uploads/slider';
            $config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';
            $config['max_size'] = 4096;
            $config['file_name'] = date('YmdHis');

            $this->upload->initialize($config);

            if ($this->upload->do_upload('file')) {
                $this->output->set_content_type('application/json')->set_output(json_encode($this->upload->data()));
            } else {
                $this->output->set_status_header(401, 'File Upload Error');
                $this->output->set_output($this->upload->display_errors());
            }
        } catch (Exception $e){
            return false;
        }
    }



    public function delete($id)
    {
        $slider = $this->slider->where('id', $id)->get();
        if ($slider) {
            if (file_exists($slider->path)) {
                unlink($slider->path);
                if ($this->slider->delete($id)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Slider Deleted']));
                } else {
                    $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Slider not deleted but some files are deleted']));
                }
            } else {
                $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Slider file not exist in directory']));
            }
        } else {
            $this->output->set_status_header(500, 'Validation error');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'The Record Not found']));
        }
    }


}