    <?php

    defined('BASEPATH') or exit('No direct script access allowed');

    class Index extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();

            $this->load->library(['email', 'pagination']);
            $this->load->model('Project_model', 'project');
            $this->load->model('Category_model', 'category');
            $this->load->model('Country_model', 'country');
            $this->load->model('Team_model', 'team');
            $this->load->model('Slider_model', 'slider');
            $this->load->model('Address_model', 'address');
            $this->load->library(['form_validation']);
        }

        function index()
        {
            $data['countries'] = $this->country->get_all();
            $data['sliders'] = $this->slider->get_all();
            $this->load->view('index', $data);
        }

        function portfolio()
        {
            $data['categories'] = $this->category->get_all();
            $data['countries'] = $this->country->get_all();
            $data['projects'] = $this->project->with_files()->with_category()->with_country()->get_all();
            $this->load->view('portfolio', $data);
        }

        public function portfolioSingle($id)
        {
            $data['next'] = $this->project->next($id);
            $data['previous'] = $this->project->previous($id);
            $data['project'] = $this->project->where('id', $id)->with_files()->with_category()->with_country()->get();
            $data['countries'] = $this->country->get_all();
            $this->load->view('portfolio-single', $data);
        }

        public function about()
        {
            $data['countries'] = $this->country->get_all();
            $data['teams'] = $this->team->get_all();
            $this->load->view('about', $data);
        }

        public function enquire()
        {
            $data['countries'] = $this->country->get_all();
            $this->load->view('enquire',$data);
        }

        public function contact()
        {
            $data['countries'] = $this->country->get_all();
            $data['addresses'] = $this->address->get_all();
            $this->load->view('contact', $data);
        }

        public function portfolioCat($id)
        {
            $data['categories'] = $this->category->get_all();
            $data['countries'] = $this->country->get_all();
            $data['projects'] = $this->project->where('country_id', $id)->with_files()->with_category()->with_country()->get_all();
            $data['currentContry'] = $this->country->where('id', $id)->get()->country;
            $this->load->view('cat-works', $data);
        }

        public function contactUs()
        {

            $config = [
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://mail.nuctaweb.com',
                'smtp_port' => 465,
                'smtp_user' => 'mail@nuctaweb.com',
                'smtp_pass' => 'mail@nucta',
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
            ];

            $this->email->initialize($config);

            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('phone', 'Name', 'required');

            if ($this->form_validation->run() === FALSE ) {
                $error = $this->form_validation->get_errors();

                $this->output->set_status_header(400, 'Validation Error');
                $this->output->set_output(json_encode($error));

            } else {

                $name = $this->input->post('name');
                $email = $this->input->post('email');
                $phone = $this->input->post('phone');
                $message = $this->input->post('comments');
                $category = $this->input->post('category');

                if ($name == 'Name') {
                    $this->output->set_output('error');
                    return false;
                    exit;
                }

                $this->email->from('mail@nuctaweb.com', 'Nucta');
                $this->email->to('nucta.ac@gmail.com');
                $this->email->subject('Contact Request From Web');


                $content = 'Name    :   ' . $name . PHP_EOL . PHP_EOL;
                $content .= 'Email    :   ' . $email . PHP_EOL . PHP_EOL;
                $content .= 'Phone   :   ' . $phone . PHP_EOL . PHP_EOL;
                $content .= 'Category   :   ' . $category . PHP_EOL . PHP_EOL;
                $content .= 'Message   :   ' . $message . PHP_EOL . PHP_EOL;

                $content = str_replace("\n.", "\n.", $content);

                $this->email->message($content);

                if ($this->email->send()) {
                    $this->output->set_output('success');
                } else {
                    $this->output->set_output('error');
                }
            }
        }

    }