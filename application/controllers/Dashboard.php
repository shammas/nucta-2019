<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('Project_model', 'project');

        $this->load->library(['encryption', 'ion_auth']);

        $this->load->helper('url');
        if (!$this->ion_auth->logged_in()) {
            redirect(base_url('login'));
        }
    }
    public function index($page="index")
    {
        $this->load->view('dashboard/' . $page);
    }
}
