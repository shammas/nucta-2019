<?php

defined('BASEPATH') or exit('No direct Script access allowed');
class Country_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
        $this->pagination_delimiters = array('<li>', '</li>', '<li class="active">');
        $this->pagination_arrows = array('Prev', 'Next');
    }

}
