<?php

defined('BASEPATH') or exit('No direct Script access allowed');
class Project_model extends MY_Model
{

    function __construct()
    {
        $this->has_many['files'] = array(
            'foreign_model' => 'project_file_model',
            'foreign_table' => 'project_files',
            'foreign_key' => 'project_id',
            'local_key' => 'id'
        );

        $this->has_one['category'] = array('foreign_model' => 'Category_model', 'foreign_table' => 'categories', 'foreign_key' => 'id', 'local_key' => 'category_id');
        $this->has_one['country'] = array('foreign_model' => 'Country_model', 'foreign_table' => 'countries', 'foreign_key' => 'id', 'local_key' => 'country_id');

        parent::__construct();
        $this->timestamps = TRUE;
        $this->pagination_delimiters = array('<li>', '</li>', '<li class="active">');
        $this->pagination_arrows = array('Prev', 'Next');
    }

    public function next($id)
    {
        $query = $this->db->query("select id,name from $this->table where id = (select min(id) from $this->table where id > $id)");
        if ($query->num_rows() > 0)
            return $query->result()[0];
        else
            return false;
    }

    public function previous($id)
    {
        $query = $this->db->query("select id,name from $this->table where id = (select max(id) from $this->table where id < $id)");
        if ($query->num_rows() > 0)
            return $query->result()[0];
        else
            return false;
    }

}
