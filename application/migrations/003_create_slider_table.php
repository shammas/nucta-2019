<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_slider_table extends CI_Migration {

    public function up()
    {

        /**
         * Table structure for table 'sliders'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'file_name' => [
                'type' => 'LONGTEXT',
                'NULL' => TRUE,
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'NULL' => TRUE,
            ],
            'path' => [
                'type' => 'LONGTEXT',
                'NULL' => TRUE,
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('sliders');

    }

    public function down()
    {
        $this->dbforge->drop_table('sliders', TRUE);
    }
}