<?php
/**
 * 002_initial_schema.php
 * Date: 17/12/18
 * Time: 02:08 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Initial_schema extends CI_Migration {

    public function up()
    {

        /**
         * Table structure for table 'countries'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'country' => [
                'type' => 'LONGTEXT',
                'NULL' => TRUE,
            ],
            'description' => [
                'type' => 'LONGTEXT',
                'NULL' => TRUE,
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('countries');


        /**
         * Table structure for table 'categories'
         *
         **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL' => TRUE,
            ],
            'description' => [
                'type' => 'LONGTEXT',
                'NULL' => TRUE,
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('categories');


         /**
         * Table structure for table 'projects'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
               'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'description' => [
               'type' => 'LONGTEXT',
                'null' => TRUE,
            ],
            'client_name' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
            ],
            'date' => [
                'type' => 'DATE',
                'null' => TRUE,
            ],
            'status' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => TRUE,
            ],
            'country_id' => [
                'type' => 'INT',
                'constraint' => '8',
                'unsigned' => TRUE,
            ],
            'category_id' => [
                'type' => 'INT',
                'constraint' => '8',
                'unsigned' => TRUE,
            ],
            'location' => [
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => TRUE,
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('projects');


        /**
         * Table structure for table 'project_files'
         *
         **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => 99,
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE,
            ],
            'path' => [
                'type' => 'LONGTEXT',
                'null' => TRUE,
            ],
            'project_id' => [
                'type'       => 'INT',
                'constraint' => '8',
                'unsigned'   => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('project_files');

        /**
         * Table structure for table 'teams'
         *
         **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'designation' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'description' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => 99,
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE,
            ],
            'path' => [
                'type' => 'LONGTEXT',
                'null' => TRUE,
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('teams');


        /**
         * Table structure for table 'ci_sessions'
         *
         **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'VARCHAR',
                'constraint' => 128,
            ],
            'ip_address' => [
                'type' => 'VARCHAR',
                'constraint' => 45,
            ],
            'timestamp' => [
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
                'default' => 0,
            ],
            'data' => [
                'type' => 'BLOB',
            ]
        ]);
        $this->dbforge->add_key(['id', 'ip_address'], TRUE);
        $this->dbforge->create_table('ci_sessions');
        $this->db->query("ALTER TABLE `ci_sessions` ADD KEY `ci_sessions_timestamp` (`timestamp`)");

    }

    public function down()
    {
        $this->dbforge->drop_table('countries', TRUE);
        $this->dbforge->drop_table('categories', TRUE);
        $this->dbforge->drop_table('projects', TRUE);
        $this->dbforge->drop_table('project_files', TRUE);
        $this->dbforge->drop_table('teams', TRUE);
    }
}