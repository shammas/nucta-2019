<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_address_table extends CI_Migration {

    public function up()
    {

        /**
         * Table structure for table 'addresses'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'place' => [
                'type' => 'LONGTEXT',
                'NULL' => TRUE,
            ],
            'address' => [
                'type' => 'LONGTEXT',
                'NULL' => TRUE,
            ],
            'phone' => [
                'type' => 'LONGTEXT',
                'NULL' => TRUE,
            ],
            'email' => [
                'type' => 'LONGTEXT',
                'NULL' => TRUE,
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('addresses');

    }

    public function down()
    {
        $this->dbforge->drop_table('addresses', TRUE);
    }
}