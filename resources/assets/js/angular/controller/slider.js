/**
 * Created by psybo-03 on 1/1/18.
 */


app.controller('SliderController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$compile',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm, DTOptionsBuilder, DTColumnDefBuilder, $compile) {

        $scope.sliders = [];
        $scope.newslider = {};
        $scope.curslider = false;
        $scope.files = [];
        $scope.errFiles = [];
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.uploaded = [];
        $scope.fileValidation = {};
        $scope.validationError = {};

        loadSlider();

        function loadSlider() {
            $http.get($rootScope.base_url + 'admin/slider').then(function (response) {
                console.log(response.data);
                if (response.data) {
                    $scope.sliders = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newSlider = function () {
            $scope.newslider = {};
            $scope.uploaded = [];
            $scope.files = [];
            $scope.errFiles = [];
            $scope.showform = true;
            $scope.item_files = false;
        };

        $scope.editSlider = function (item) {
            $scope.showform = true;
            $scope.curslider = item;
            $scope.newslider = angular.copy(item);
            $scope.files = [];
        };

        $scope.hideForm = function () {
            $scope.errFiles = [];
            $scope.showform = false;
        };

        $scope.addSlider = function () {

            var fd = new FormData();

            angular.forEach($scope.newslider, function (item, key) {
                fd.append(key, item);
            });

            fd.append('uploaded', JSON.stringify($scope.uploaded));

            if ($scope.newslider['id']) {
                var url = $rootScope.base_url + 'admin/slider/update/' + $scope.newslider.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        //angular.extend($scope.sliders, $scope.curslider, response.data);
                        loadSlider();
                        $scope.newslider = {};
                        $scope.showform = false;
                        $scope.files = '';
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                        $scope.files = '';
                    });
            } else {
                var url = $rootScope.base_url + 'admin/slider/store';
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        //$scope.sliders.push(response.data);
                        loadSlider();
                        $scope.newslider = {};
                        $scope.showform = false;
                        $scope.files = [];

                    }, function onError(response) {
                        console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                        console.log(response.data);
                        $scope.validationError = response.data;
                        $scope.files = [];
                    });
            }
        };

        $scope.deleteSlider = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'admin/slider/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    //var index = $scope.sliders.indexOf(item);
                                    //$scope.sliders.splice(index, 1);
                                    loadSlider();
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };


        $scope.uploadFiles = function (files, errFiles) {
            angular.forEach(errFiles, function (errFile) {
                $scope.errFiles.push(errFile);
            });
            angular.forEach(files, function (file) {
                $scope.files.push(file);
                file.upload = Upload.upload({
                    url: $rootScope.base_url + 'admin/slider/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        $scope.uploaded.push(response.data);
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
                });
            });
        };

        $scope.deleteImage = function (item) {

            var url = $rootScope.base_url + 'admin/slider/delete-image/' + item['id'];
            $http.delete(url)
                .then(function onSuccess(response) {
                    console.log('image deleted');
                    $scope.item_files = '';
                }, function onError(response) {
                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                    console.log(response.data);
                });
        };

        $scope.showSliderFiles = function (item) {
            console.log(item);
            $scope.sliderfiles = item;
        };

    }]);

