app.controller('CountryController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$compile',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm, DTOptionsBuilder, DTColumnDefBuilder, $compile) {

        $scope.countries = [];
        $scope.newcountry = {};
        $scope.curcountry = false;
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.validationError = {};

        loadCountry();


        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDOM('gl<"#add-btn.col-xs-12 col-sm-3 col-md-3 text-center">ft<"col-sm-6"i>p')
            .withDisplayLength(10)
            .withOption('stripHtml', false)
            .withOption('lengthMenu', $rootScope.lengthMenu)
            .withOption('initComplete', function () {
                var myEl = angular.element(document.querySelector('#add-btn'));
                myEl.append($compile('<button type="button" class="btn btn-primary btn-xs" ng-click="newCountry()">Add a Country</button>')($scope));
            });

        function loadCountry() {
            $http.get($rootScope.base_url + 'admin/country').then(function (response) {
                if (response.data) {
                    $scope.countries = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newCountry = function () {
            $scope.newcountry = {};
            $scope.showform = true;
        };

        $scope.editCountry = function (item) {
            $scope.showform = true;
            $scope.curcountry = item;
            $scope.newcountry = angular.copy(item);
        };

        $scope.hideForm = function () {
            $scope.showform = false;
        };

        $scope.addCountry = function () {

            var fd = new FormData();

            angular.forEach($scope.newcountry, function (item, key) {
                fd.append(key, item);
            });


            if ($scope.newcountry['id']) {
                var url = $rootScope.base_url + 'admin/country/update/' + $scope.newcountry.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        angular.extend($scope.countries, $scope.curcountry, response.data);
                        $scope.newcountry = {};
                        $scope.showform = false;
                        loadCountry();
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                    });
            } else {
                var url = $rootScope.base_url + 'admin/country/store';
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        $scope.countries.push(response.data);
                        $scope.newproject = {};
                        $scope.showform = false;

                    }, function onError(response) {
                        console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                    });
            }
        };

        $scope.deleteCountry = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'admin/country/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    var index = $scope.countries.indexOf(item);
                                    $scope.countries.splice(index, 1);
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };
    }]);

