app.controller('CategoryController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$compile',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm, DTOptionsBuilder, DTColumnDefBuilder, $compile) {

        $scope.categories = [];
        $scope.newcategory = {};
        $scope.curcategory = false;
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.validationError = {};

        loadCategory();


        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDOM('gl<"#add-btn.col-xs-12 col-sm-3 col-md-3 text-center">ft<"col-sm-6"i>p')
            .withDisplayLength(10)
            .withOption('stripHtml', false)
            .withOption('lengthMenu', $rootScope.lengthMenu)
            .withOption('initComplete', function () {
                var myEl = angular.element(document.querySelector('#add-btn'));
                myEl.append($compile('<button type="button" class="btn btn-primary btn-xs" ng-click="newCategory()">Add a Category</button>')($scope));
            });

        function loadCategory() {
            $http.get($rootScope.base_url + 'admin/category').then(function (response) {
                if (response.data) {
                    $scope.categories = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newCategory = function () {
            $scope.newcategory = {};
            $scope.showform = true;
        };

        $scope.editCategory = function (item) {
            $scope.showform = true;
            $scope.curcategory = item;
            $scope.newcategory = angular.copy(item);
        };

        $scope.hideForm = function () {
            $scope.showform = false;
        };

        $scope.addCategory = function () {

            var fd = new FormData();

            angular.forEach($scope.newcategory, function (item, key) {
                fd.append(key, item);
            });


            if ($scope.newcategory['id']) {
                var url = $rootScope.base_url + 'admin/category/update/' + $scope.newcategory.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        angular.extend($scope.categories, $scope.curcategory, response.data);
                        $scope.newcategory = {};
                        $scope.showform = false;
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                    });
            } else {
                var url = $rootScope.base_url + 'admin/category/store';
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        $scope.categories.push(response.data);
                        $scope.newproject = {};
                        $scope.showform = false;

                    }, function onError(response) {
                        console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                    });
            }
        };

        $scope.deleteCategory = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'admin/category/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    var index = $scope.categories.indexOf(item);
                                    $scope.categories.splice(index, 1);
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };
    }]);

