app.controller('AddressController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$compile',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm, DTOptionsBuilder, DTColumnDefBuilder, $compile) {

        $scope.addresses = [];
        $scope.newaddress = {};
        $scope.curaddress = false;
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.validationError = {};

        loadAddress();


        $scope.dtOptions = DTOptionsBuilder.newOptions()
            .withDOM('gl<"#add-btn.col-xs-12 col-sm-3 col-md-3 text-center">ft<"col-sm-6"i>p')
            .withDisplayLength(10)
            .withOption('stripHtml', false)
            .withOption('lengthMenu', $rootScope.lengthMenu)
            .withOption('initComplete', function () {
                var myEl = angular.element(document.querySelector('#add-btn'));
                myEl.append($compile('<button type="button" class="btn btn-primary btn-xs" ng-click="newAddress()">Add a Address</button>')($scope));
            });

        function loadAddress() {
            $http.get($rootScope.base_url + 'admin/address').then(function (response) {
                if (response.data) {
                    $scope.addresses = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newAddress = function () {
            $scope.newaddress = {};
            $scope.showform = true;
        };

        $scope.editAddress = function (item) {
            $scope.showform = true;
            $scope.curaddress = item;
            $scope.newaddress = angular.copy(item);
        };

        $scope.hideForm = function () {
            $scope.showform = false;
        };

        $scope.addAddress = function () {

            var fd = new FormData();

            angular.forEach($scope.newaddress, function (item, key) {
                fd.append(key, item);
            });


            if ($scope.newaddress['id']) {
                var url = $rootScope.base_url + 'admin/address/update/' + $scope.newaddress.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        angular.extend($scope.addresses, $scope.curaddress, response.data);
                        $scope.newaddress = {};
                        $scope.showform = false;
                        loadAddress();
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                    });
            } else {
                var url = $rootScope.base_url + 'admin/address/store';
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        $scope.addresses.push(response.data);
                        $scope.newproject = {};
                        $scope.showform = false;

                    }, function onError(response) {
                        console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                    });
            }
        };

        $scope.deleteAddress = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'admin/address/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    var index = $scope.addresses.indexOf(item);
                                    $scope.addresses.splice(index, 1);
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };
    }]);

