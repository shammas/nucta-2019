/**
 * Created by psybo-03 on 12/12/17.
 */

var app = angular.module('myApp', ['ngRoute', 'ui.bootstrap', 'ngFileUpload', 'cp.ngConfirm', 'angular-loading-bar', 'datatables', 'datatables.buttons', 'datatables.colreorder', 'datatables.fixedheader']);
app.config(['$routeProvider', '$locationProvider', 'cfpLoadingBarProvider','$windowProvider','$httpProvider', function ($routeProvider, $locationProvider, cfpLoadingBarProvider,$windowProvider,$httpProvider) {


    var $window = $windowProvider.$get();
    $httpProvider.interceptors.push(["$q", "$location", "$window", function ($q, $location, $window) {
        return {
            request: function (config) {
                config.headers = config.headers || {};
                return config;
            },
            response: function (response) {
                if (response.statusText == "Unauthenticated") {
                    $window.location.href = '/login';
                }
                return response || $q.when(response);
            }
        };
    }]);

    cfpLoadingBarProvider.spinnerTemplate = '<div id="loading"></div>';
    cfpLoadingBarProvider.latencyThreshold = 500;

    $locationProvider.hashPrefix('');
    $routeProvider
        .when('/', {
            templateUrl: 'dashboard/index/dashboard'
        })
        .when('/dashboard', {
            templateUrl: 'dashboard/index/dashboard',
            controller: 'DashboardController'
        })
        .when('/projects', {
            templateUrl: 'dashboard/index/projects',
            controller: 'ProjectController'
        })
        .when('/category', {
            templateUrl: 'dashboard/index/category',
            controller: 'CategoryController'
        })
        .when('/country', {
            templateUrl: 'dashboard/index/country',
            controller: 'CountryController'
        })
        .when('/team', {
            templateUrl: 'dashboard/index/team',
            controller: 'TeamController'
        })
        .when('/slider', {
            templateUrl: 'dashboard/index/slider',
            controller: 'SliderController'
        })
        .when('/address', {
            templateUrl: 'dashboard/index/address',
            controller: 'AddressController'
        })
        .otherwise({
            templateUrl: 'dashboard/index/dashboard'
        });

}]);


